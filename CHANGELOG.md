# Changelog

Author: Sascha Möser - [uneart.de](http://uneart.de)

******************

**0.0.1 (January 18, 2015)**
- Initial release of the plugin.