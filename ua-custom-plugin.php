<?php

if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Plugin Name: uneart Functions Plugin
 * Plugin URI: http://uneart.de
 * Description: uneart WordPress plugin for varius theme-independant functionalities.
 * Version: 0.0.1.
 * Author: Sascha Möser
 * Author URI: http://uneart.de
 * License: GPL2
 *
 *
 * This plugin is released under the GPLv2 license. The images packaged with this plugin are the property of
 * their respective owners, and do not, necessarily, inherit the GPLv2 license.
*/

/**
 * Define some useful constants
 **/
define('UA_THEMEPLUGIN_VERSION', '1.0');
define('UA_THEMEPLUGIN_DIR', plugin_dir_path(__FILE__));
define('UA_THEMEPLUGIN_URL', plugin_dir_url(__FILE__));


/**
 * Load files
 *
 **/
function ua_themeplugin_load(){

    // Developer Tools
    // require_once( CHILD_DIR . '/includes/developer-tools.php' );     // DO NOT USE THESE ON A LIVE SITE

    // Admin
    require_once( UA_THEMEPLUGIN_DIR . '/inc/admin/admin-functions.php' );  // Customization to admin functionality
    require_once( UA_THEMEPLUGIN_DIR . '/inc/admin/admin-views.php' );      // Customizations to the admin area display
    require_once( UA_THEMEPLUGIN_DIR . '/inc/admin/admin-branding.php' );   // Admin view customizations that specifically involve branding

    // Shame
    require_once( UA_THEMEPLUGIN_DIR . '/inc/shame.php' );                  // For new code snippets that haven't been sorted and commented yet
}

ua_themeplugin_load();

?>