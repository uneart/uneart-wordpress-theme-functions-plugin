<?php

if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Change WP JPEG compression (WP default is 90%)
 *
 * See: http://wpmu.org/how-to-change-jpeg-compression-in-wordpress/
 *
 */
// add_filter( 'jpeg_quality', create_function( '', 'return 80;' ) );


/**
 * Activate the Link Manager
 *
 * See: http://wordpressexperts.net/how-to-activate-link-manager-in-wordpress-3-5/
 *
 */
// add_filter( 'pre_option_link_manager_enabled', '__return_true' );		// Activate

/**
 * Disable pingbacks
 *
 * See: http://wptavern.com/how-to-prevent-wordpress-from-participating-in-pingback-denial-of-service-attacks
 *
 * Still having pingback/trackback issues? This post might help: https://wordpress.org/support/topic/disabling-pingbackstrackbacks-on-pages#post-4046256
 *
 */
add_filter( 'xmlrpc_methods', 'uatp_remove_xmlrpc_pingback_ping' );
function uatp_remove_xmlrpc_pingback_ping( $methods ) {

	unset($methods['pingback.ping']);
	return $methods;

};

/**
 * Disable XML-RPC
 *
 * See: https://wordpress.stackexchange.com/questions/78780/xmlrpc-enabled-filter-not-called
 *
 */
// if( defined( 'XMLRPC_REQUEST' ) && XMLRPC_REQUEST ) exit;

/**
 * Automatically remove readme.html (and optionally xmlrpc.php) after a WP core update
 *
 */
add_action( '_core_updated_successfully', 'uatp_remove_files_on_upgrade' );
function uatp_remove_files_on_upgrade() {

	if( file_exists(ABSPATH . 'readme.html') )
		unlink(ABSPATH . 'readme.html');

	// if( file_exists(ABSPATH . 'xmlrpc.php') )
	// 	unlink(ABSPATH . 'xmlrpc.php');

}
