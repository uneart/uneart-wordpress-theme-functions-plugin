<?php

if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_filter( 'login_headerurl', 'uatp_login_headerurl' );
/**
 * Login screen's logo links to homepage, instead of WordPress.org.
 *
 */
function uatp_login_headerurl() {

    return home_url();

}

add_filter( 'login_headertitle', 'uatp_login_headertitle' );
/**
 * Makes the login screen's logo title attribute your site title, instead of 'WordPress'.
 *
 */
function uatp_login_headertitle() {

    return get_bloginfo( 'name' );

}

add_filter( 'wp_mail_from_name', 'uatp_mail_from_name' );
/**
 * Makes WordPress-generated emails appear 'from' your WordPress site name, instead of from 'WordPress'.
 *
 */
function uatp_mail_from_name() {

    return get_option( 'blogname' );

}

// add_filter( 'wp_mail_from', 'uatp_wp_mail_from' );
/**
 * Makes WordPress-generated emails appear 'from' your WordPress admin email address.
 *
 * Disabled by default, in case you don't want to reveal your admin email.
 *
 */
function uatp_wp_mail_from() {

    return get_option( 'admin_email' );

}

// add_filter( 'mandrill_payload', 'uatp_force_mandrill_payload_to_html' );
/**
 * Mandrill sends all emails as HTML. Wrap all plaintext content in <p> tags.
 *
 * See: http://wordpress.org/support/topic/plaintext-emails-converted-to-html-remove-newlines
 *
 */
function uatp_force_mandrill_payload_to_html( $message ) {

    $message['html'] = wpautop($message['html']);
    return $message;

}

add_filter( 'retrieve_password_message', 'uatp_cleanup_retrieve_password_message' );
/**
 * Remove the brackets from the retreive PW link, since they get hidden on HTML
 *
 */
function uatp_cleanup_retrieve_password_message( $message ) {

    return preg_replace( '/<(.+?)>/', '$1', $message );

}

add_action( 'wp_before_admin_bar_render', 'uatp_remove_wp_icon_from_admin_bar' );
/**
 * Removes the WP icon from the admin bar
 *
 * See: http://wp-snippets.com/remove-wordpress-logo-admin-bar/
 *
 */
function uatp_remove_wp_icon_from_admin_bar() {

    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');

}

// add_filter( 'admin_footer_text', 'uatp_admin_footer_text' );
/**
 * Modify the admin footer text
 *
 * See: http://wp-snippets.com/change-footer-text-in-wp-admin/
 *
 */
function uatp_admin_footer_text() {

    return 'YOUR TEXT HERE.';

}
